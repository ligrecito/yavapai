windows_agent: file() | mem(size=10mb) | avro(12.23.12.22:1000)
linux_agent: file() | mem(size=10mb) | avro(12.23.12.22:1000)

// Example of fan out
consolidator_agent: avro(localhost:1000) 
										 -> hdfs(hdfs://data//YYYYMMDDHHMMSS.log) if (header=value & header=value)
										 -> hdfs(hdfs://data/lab.log) if (header=value)
										 -> logger otherwise




-----
FsShell class mimic the functionality of command line shell, meant to be used programatically. (source spring hadoop)

------

file(path=>//as0321/install/logs/*) | filter(pattern=>"security*.log") | file(path=>hfds://data//logs)

twitter(query=>Bieber)@(cron | Duration) | file


TODO
----
- from http with bind="localhost", port=9999 to logger
- http(localhost:999) -> logger

- file(c:\\logs) with filter(name=security*.log)| jdbc | logger

- netcat(bind=localhost,port=999,size=10MB) | hdfs(hdfs://data//YYYYMMDDHHMMSS.log)
- netcat(bind=localhost,port=999,size=10MB) | filter(pattern=> localhost) | 
- 

(http)*2 | #processingModule(filter|transformer) | file

#processingModule = machine1, machine2

Type1 = for (logs <- file(path=>//as0321/install/logs/*).filter(pattern=>"security*.log".map(file(path)))
Type2 (like DOT(Graph Description language)) 

akkaSystem reliableProxy {
	twitter(query=>"Bieber")@10 secs -> file (path=>hfds://data//logs/twitter) 
} 
