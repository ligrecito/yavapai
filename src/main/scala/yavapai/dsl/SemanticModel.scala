package yavapai.dsl

/**
 * Semantic Model supporting our combinators to build the AST.
 */
object SemanticModel {
  trait Property
  case class StringProperty(key:String, value: String) extends Property
  case class IntProperty(key:String, value: Int) extends Property
  case class BooleanProperty(key: String, value: Boolean) extends Property

  trait ComponentType
  case object SOURCE extends ComponentType
  case object CHANNEL extends ComponentType
  case object SINK extends ComponentType

  case class Component(name: String, componentType: ComponentType, properties: Seq[Property])
  case class PipelineFlow(flow: Seq[Component])
}
