package yavapai.dsl
import Dsl._
/**
 * User: javierteso
 * Date: 15/05/13
 */
object Main extends App{


  var pipelines = List (
    """
      netcat(bind="localhost",port=9292,max_line_length=512) -> logger
    """,
    """
      seq -> logger
    """
  )

  // Testing
  pipelines.foreach( _.printAST )

 }
