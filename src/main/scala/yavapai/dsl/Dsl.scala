package yavapai.dsl

import scala.util.parsing.combinator.syntactical._

/**
 * Yavapai is an external DSL for Apache Flume using Parser combinators in Scala.
 *
 * This class defines the grammar rules for our DSLs
 * User: javierteso
 * Date: 15/05/13
 */
object Dsl extends StandardTokenParsers{
  import SemanticModel._

  lexical.reserved += ("http", "logger", "seq", "netcat", "exec", "syslogtcp", "syslogudp", "hdfs", "memory", "jdbc", "file",
    "max_line_length", "ack_every_event", "bind", "port")  // TODO - to be completed

  lexical.delimiters += ("->", "(", ")", "=", ",")

  lazy val flumePipeline : Parser[PipelineFlow] =
    rep1sep(command_spec, "->")  ^^ {case f: Seq[Component] => PipelineFlow(f) }

  lazy val command_spec =
    netcat_spec | sequence_spec | logger_spec

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //  Sources
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * -------------
   * Netcat parser
   * -------------
   */
  lazy val netcat_spec: Parser[Component] =
    "netcat" ~ ("(" ~> netcat_properties_sec <~ ")") ^^
      {case n ~ s => Component(n, SOURCE, s)}  //netcat~List( (p~v), (p~v), ...)

  lazy val netcat_properties_sec: Parser[Seq[Property]] =
    repsep( (bind_property | port_property | max_line_length_property | ack_every_event_property), "," ) ^^
      { case p: Seq[Property] => p }

  lazy val max_line_length_property =
    "max_line_length" ~ ( "=" ~> sizeInBytes) ^?
      ({ case k ~ v if v.toInt > 0 => IntProperty(k, v.toInt) },
            (m => "[max_line_length] property needs to be an integer greater than zero." ))

  lazy val ack_every_event_property =
    "ack_every_event" ~ ( "=" ~> boolean) ^^ { case k ~ v => BooleanProperty(k, v.toBoolean)}

  lazy val bind_property   =
    "bind" ~ ("=" ~> hostname_OR_ip_address) ^^ { case k ~ v => StringProperty(k,v)}

  lazy val port_property   =
    "port" ~ ("=" ~> port ) ^^ { case k ~ v => IntProperty(k,v.toInt)}

  /**
   * ----------------
   * Sequence parser
   * ----------------
   */
  lazy val sequence_spec: Parser[Component] = "seq" ^^^  Component("seq", SOURCE, Nil)

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //  Sinkers
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /**
   * ---------------
   * Logger
   * ---------------
   * Logs events at INFO level. Typically useful for testing/debugging purposes.
   */
  lazy val logger_spec: Parser[Component] = "logger" ^^^ Component("logger", SINK, Nil)

  /**
   * Primitive parsers
   */
  lazy val hostname_OR_ip_address = stringLit
  lazy val port = numericLit
  lazy val sizeInBytes = numericLit
  lazy val boolean = "true" | "false"

  implicit def string2FlowDefinition(x: String) = new FlowDefinition(x)
}

class FlowDefinition(flow: String){

  def printAST() {
    import Dsl._

    flumePipeline(new lexical.Scanner(flow)) match {
      case Success(yavapai, _) => println(yavapai)
      case Failure(msg, _) => println("Failure:" + msg)
      case Error(msg, _) => println("Error:" + msg)
    }
  }
}
