name := "scala-xd"
 
version := "0.1"
 
scalaVersion := "2.10.0"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")
 
resolvers ++= Seq(
  "akka" at "http://mvnrepository.com"
)

libraryDependencies ++= Seq(
  "org.apache.hadoop"   %   "hadoop-core"     % "0.20.2",
  "org.apache.hadoop"   %   "hadoop-common"   % "0.22.0",
  "org.apache.hadoop"   %   "hadoop-hdfs"     % "0.22.0", 
  "org.specs2"          %%  "specs2"          % "1.13" % "test"         
)

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

seq(Revolver.settings: _*)

